FROM golang:1.22-bookworm

WORKDIR /

ENV GO111MODULE=on \
    CGO_ENABLED=0

RUN apt-get update &&\
    apt-get install -y --no-install-recommends ca-certificates curl wget gnupg lsb-release &&\
    curl -fsSL https://deb.nodesource.com/setup_18.x | bash - &&\
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker.gpg &&\
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list &&\
    apt-get update &&\
    apt-get install -y build-essential libgtk-3-dev libwebkit2gtk-4.0-dev nodejs docker-ce docker-ce-cli containerd.io make &&\
    apt-get -y autoremove --purge &&\
    rm -rf /var/lib/apt/lists/*

RUN go install github.com/wailsapp/wails/v2/cmd/wails@latest

CMD ["wails"]
