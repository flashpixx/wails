PHONY: version install deploy

# gets the current version
version:
	@docker run --rm -v $(PWD):/src gittools/gitversion:latest-5.0 /src

# container build
build:
	@docker compose config
	@docker compose build

# deploy container
deploy:
	@docker compose push
