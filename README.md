# Wails

Regularly updated linux-based Golang 1.22 Debian Bookworm [Wails (bleeding edge)](https://wails.app/) container image with build tools.

### Worflow

1. run the image with

   ```
   docker run --rm -it -v <path to your code>:/src flashpixx/wails <wails command>
   ```

2. wails command will be executed
